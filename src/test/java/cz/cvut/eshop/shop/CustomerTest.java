package cz.cvut.eshop.shop;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CustomerTest {

	@Test
	public void addLoyaltyPoints() {
		Customer customer = new Customer(8);
        customer.addLoayltyPoint(2);
        
        assertEquals(10, customer.getLoyaltyPoints());
	}
	
	@Test
	public void setCustomerAddress() {
		Customer customer = new Customer(0);
		customer.setCustomerAddress("Bělehradská 31, 120 00 Praha");
		
		assertEquals("Bělehradská 31, 120 00 Praha", customer.getCustomerAddress());
	}
	
	
}
