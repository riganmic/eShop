package cz.cvut.eshop.shop;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class DiscountedItemTest {

	@Test
	public void getPrice_shouldReturnDiscountedPrice() {
		DiscountedItem item = new DiscountedItem(1, "banana", 3.60f, "fruit", 30, "1.11.2018", "8.11.2018");
		
		assertEquals(2.52f, item.getPrice(), 0.05f);
	}
	
}
