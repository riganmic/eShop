package cz.cvut.eshop.shop;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;

public class EShopControllerTest {

	@Test
	public void startEShop() {
		assertNull(EShopController.storage);
		EShopController.startEShop();

		assertNotNull(EShopController.storage);
		assertNotNull(EShopController.archive);
		assertNotNull(EShopController.carts);
		assertNotNull(EShopController.orders);

	}

}
